package edu.utdallas.cs6378.dfs.messages;

public enum FileSystemMessageType {
    REQUEST_LOCK,
    ASSIGN_VOTE,            // Give votes to node requesting lock
    RETRIEVE_FILE,          // Retrieve most recent file
    FILE_CONTENT,           // Respond with most recent file content
    SET_FILE,               // Create or update file content
    SET_FILE_ACK,           // Acknowledge receipt of file content update
    RELEASE_LOCK,           // Release lock on the file
    RELEASE_LOCK_ACK,       // Acknowledge receipt of RELEASE_LOCK
    ABORT                   // Rollback writes and release locks

}
