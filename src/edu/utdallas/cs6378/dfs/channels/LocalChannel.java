package edu.utdallas.cs6378.dfs.channels;

import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Local channel class designed for debugging and testing. Messages are not sent across any network but are stored
 * in blocking queues.
 */
public class LocalChannel extends Channel {
    public LinkedBlockingQueue<FileSystemMessage> OutgoingMessages;
    public LinkedBlockingQueue<FileSystemMessage> IncomingMessages;

    private boolean open = true;

    public LocalChannel(){
        OutgoingMessages = new LinkedBlockingQueue<>();
        IncomingMessages = new LinkedBlockingQueue<>();
    }

    @Override
    public void send(FileSystemMessage msg) throws IOException {
        try {
            OutgoingMessages.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public FileSystemMessage recv() throws IOException, InterruptedException {

        return IncomingMessages.take();
    }

    @Override
    public void close() throws IOException {
        open = false;
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    @Override
    public void run() {

    }
}
