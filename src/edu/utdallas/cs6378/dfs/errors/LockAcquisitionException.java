package edu.utdallas.cs6378.dfs.errors;

/**
 * Created by mark on 7/22/14.
 */
public class LockAcquisitionException extends Exception {
    public LockAcquisitionException(){
        super("The lock could not be acquired for an unknown reason.");
    }

    public LockAcquisitionException(String message){
        super(message);
    }
    public LockAcquisitionException(String message, Throwable cause){
        super(message, cause);
    }
}
