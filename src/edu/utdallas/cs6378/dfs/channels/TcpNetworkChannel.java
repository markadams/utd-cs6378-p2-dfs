package edu.utdallas.cs6378.dfs.channels;

import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * TCP-based network channel. Sends and receives messages using TCP sockets
 */
public class TcpNetworkChannel extends Channel {
    private final int port;
    private ServerSocket socket;
    private boolean listening;
    private LinkedBlockingQueue<FileSystemMessage> incomingQueue = new LinkedBlockingQueue<FileSystemMessage>();
    private Thread receiver;

    public static final int DEFAULT_TCP_PORT = 5454;
    private static Logger logger = LogManager.getLogger("TCP");

    public TcpNetworkChannel(int port) throws IOException {
        this.port = port;
        this.socket = new ServerSocket(port);
        this.receiver = new Thread(this);
        this.receiver.start();
    }


    public void send(FileSystemMessage msg) throws IOException {
        String[] addrParts = msg.getToAddress().split(":");

        InetAddress address;
        int port;

        address = InetAddress.getByName(addrParts[0].replace("/",""));
        port = Integer.parseInt(addrParts[1]);

        if (!msg.getFromAddress().endsWith(String.valueOf(this.port))){
            msg.setFromAddress(msg.getFromAddress() + ":" + this.port);
        }

        try{
            logger.debug(String.format("SEND %s TO %s FOR %s", msg.getMessageType(), msg.getToAddress(), msg.getFilename()));

            Socket client = new Socket(address, port);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));

            writer.write(msg.toJSON());
            writer.close();
            client.close();
        }catch (ConnectException ex){

        }

    }

    @Override
    public FileSystemMessage recv() throws IOException {
        if ((this.socket.isClosed()) && incomingQueue.size() == 0){
            throw new IOException("Channel has already been closed and no more messages remain to be received.");
        }

        try{
            return incomingQueue.take();
        } catch (InterruptedException e) {
            return null;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.socket == null){
            return;
        }

        if (!this.socket.isClosed()){
            this.socket.close();
        }

        if (this.receiver.isAlive()){
            this.receiver.interrupt();
        }
    }

    @Override
    public boolean isOpen() {
        return !this.socket.isClosed();
    }

    @Override
    public void run() {
        try {
            while (!socket.isClosed()) {
                Socket sock = socket.accept();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(sock.getInputStream()));

                String body = in.readLine();

                FileSystemMessage msg = FileSystemMessage.fromJSON(body);
                logger.debug(String.format("RECV %s FROM %s FOR %s", msg.getMessageType(), msg.getFromAddress(), msg.getFilename()));

                incomingQueue.add(msg);
                sock.close();
            }
        } catch (SocketException e){
            if (!e.getMessage().equals("Socket closed")){
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if (this.isOpen()){
                    this.close();
                }

            } catch (IOException e) {

            }
        }
    }
}
