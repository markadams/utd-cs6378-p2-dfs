package edu.utdallas.cs6378.dfs.config;

import edu.utdallas.cs6378.dfs.channels.TcpNetworkChannel;
import edu.utdallas.cs6378.dfs.filesystem.Peer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mark on 8/2/14.
 */
public class ConfigFile implements Iterator<ConfigFileLine>, Iterable<ConfigFileLine> {
    File inputFile;
    BufferedReader reader;
    String hostname;
    Integer port;
    Integer nodeVotes;
    ArrayList<Peer> peers;
    private int r;
    private int w;

    public ConfigFile(String filename) throws IOException {
        inputFile = new File(filename);

        if (!inputFile.exists()){
            throw new IOException("The specified configuration file does not exist");
        }

        initReader();
    }

    public String getName(){
        return hostname;
    }

    public String getAddress() {
        return hostname + ":" + getPort();
    }

    public Integer getPort(){
        return (port != null) ? port : TcpNetworkChannel.DEFAULT_TCP_PORT;
    }

    public Integer getVotes(){
        return nodeVotes;
    }

    public List<Peer> getPeers(){
        return peers;
    }

    public Integer getW(){
        return w;
    }

    public Integer getR(){
        return r;
    }

    private void initReader() throws IOException {
        reader = new BufferedReader(new FileReader(inputFile));
        String[] parts = reader.readLine().split("\t");

        String nodeId = parts[0];
        String[] nodeIdParts = nodeId.split(":");

        hostname = nodeIdParts[0];

        if (nodeIdParts.length == 2){
            port = Integer.parseInt(nodeIdParts[1]);
        }

        nodeVotes = Integer.parseInt(parts[1].trim());

        parts = reader.readLine().trim().split("\t");
        peers = new ArrayList<Peer>();

        for (int i = 0; i < parts.length; i = i + 2){
            String[] peerParts = parts[i].split(":");
            String peerId = peerParts[0];

            int peerPort = Integer.parseInt(peerParts[1]);

            int votes = Integer.parseInt(parts[i+1].trim());
            peers.add(new Peer(new InetSocketAddress(peerId, peerPort), votes));
        }

        parts = reader.readLine().trim().split("\t");
        assert parts[0].equals("params");

        r = Integer.parseInt(parts[1]);
        w = Integer.parseInt(parts[2]);
    }

    @Override
    public boolean hasNext() {
        try {
            return reader.ready();
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public ConfigFileLine next() {
        try {
            ConfigFileLine line = new ConfigFileLine(reader.readLine());
            return line;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<ConfigFileLine> iterator() {
        return this;
    }
}
