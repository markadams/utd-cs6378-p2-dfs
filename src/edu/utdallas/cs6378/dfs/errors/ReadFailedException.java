package edu.utdallas.cs6378.dfs.errors;

/**
 * Created by mark on 7/22/14.
 */
public class ReadFailedException extends Exception {
    public ReadFailedException(String message){
        super(message);
    }
    public ReadFailedException(String message, Throwable cause){
        super(message, cause);
    }
}
