'''
Usage: python sanity_check.py localhost:5454.log localhost:5455.log localhost:5456.log
'''

import sys

def format_log(inf):
    results = []
    for line in inf:
        tup = line.strip().split('\t')
        results.append((tup[0], int(tup[1]), tup[2]))

    return results

all_nodes = []

node1 = format_log(open(sys.argv[1]).readlines())
node2 = format_log(open(sys.argv[2]).readlines())
node3 = format_log(open(sys.argv[3]).readlines())

all_nodes.extend(node1)
all_nodes.extend(node2)
all_nodes.extend(node3)

unique_entries = set(all_nodes)

files = set([tup[0] for tup in all_nodes])
max_version = {}

for file in files:
    max_version[file] = max([tup[1] for tup in all_nodes if tup[0] == file])

for file in files:

    file_entries = filter(lambda x: x[0] == file, unique_entries)
    unique_versions = set([entry[1] for entry in file_entries])


    if len(file_entries) != len(unique_versions):
        last = None
        last_version = -1

        for entry in sorted(file_entries):
            if entry[1] == last_version:
                print last
                print entry

            last = entry
            last_version = entry[1]
