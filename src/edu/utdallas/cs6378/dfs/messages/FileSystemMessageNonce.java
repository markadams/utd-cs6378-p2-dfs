package edu.utdallas.cs6378.dfs.messages;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class FileSystemMessageNonce {
	private static MessageDigest fsMessageDigest ;

	private static void setAlgo(String algo) throws NoSuchAlgorithmException {
		
		fsMessageDigest = MessageDigest.getInstance(algo);
	}
	
	public static String getNonce(){
		try
		{
			setAlgo("MD5");
			Random r = new Random(446476567);			
			return fsMessageDigest.digest(String.valueOf(r.nextLong()).getBytes()).toString();
		}
		catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
	
}
