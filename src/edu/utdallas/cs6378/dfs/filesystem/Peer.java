package edu.utdallas.cs6378.dfs.filesystem;

import java.net.InetSocketAddress;

/**
 * Created by mark on 7/22/14.
 */
public class Peer {
    private InetSocketAddress peerAddress;
    private int votes;

    public Peer(InetSocketAddress peerAddress, int votes){
        this.peerAddress = peerAddress;
        this.votes = votes;
    }

    public String getAddress(){
        return this.peerAddress.getHostString() + ":" + this.peerAddress.getPort();
    }

    public int getVotes(){
        return votes;
    }
}
