package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.filesystem.FileHandle;

import java.io.IOException;

/**
 * Created by mark on 7/30/14.
 */
public class CloseFileTestThread implements Runnable {
    public FileHandle handle;
    private boolean result = false;
    public CloseFileTestThread(FileHandle handle){
        this.handle = handle;
    }

    public synchronized boolean getResult(){
        return result;
    }
    
    @Override
    public synchronized void run() {
        try {
            handle.close();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LockAcquisitionException e) {
            e.printStackTrace();
        }

    }
}
