package edu.utdallas.cs6378.dfs.tests;

import java.io.IOException;

import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.filesystem.FileHandle;
import edu.utdallas.cs6378.dfs.filesystem.FileMode;
import edu.utdallas.cs6378.dfs.filesystem.FileSystem;

public class OpenFileTestThread implements Runnable{
	public FileSystem fs;
	private String filename;
	private FileMode mode;
    private boolean result = false;
    public OpenFileTestThread(FileSystem fs, String filename, FileMode mode){
        this.fs = fs;
        this.filename = filename;
        this.mode = mode;
    }

    public synchronized boolean getResult(){
        return result;
    }
    
    @Override
    public synchronized void run() {
        try {
        	fs.openFile(filename, mode);
            result = true;
        } catch (LockAcquisitionException e) {
            e.printStackTrace();
        }

    }
}
