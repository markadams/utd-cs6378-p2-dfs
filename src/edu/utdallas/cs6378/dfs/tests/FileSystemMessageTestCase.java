package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by mark on 7/2/14.
 */
public class FileSystemMessageTestCase {
    @Test
    public void JsonDeserializedShouldMatchOriginal() throws IOException {
        FileSystemMessage message = FileSystemMessage.CreateFileContent("test_file.dat", "127.0.0.1", "127.0.0.2", "Hello World!", 10);

        String jsonMessage = message.toJSON();

        FileSystemMessage unpacked = FileSystemMessage.fromJSON(jsonMessage);

        assertEquals(message.getFilename(), unpacked.getFilename());
        assertEquals(message.getFromAddress(), unpacked.getFromAddress());
        assertEquals(message.getToAddress(), unpacked.getToAddress());
        assertEquals(message.getContent(), unpacked.getContent());
        assertEquals(message.getVersion(), unpacked.getVersion());

    }
}
