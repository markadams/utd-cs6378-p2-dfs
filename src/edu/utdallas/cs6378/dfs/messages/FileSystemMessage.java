package edu.utdallas.cs6378.dfs.messages;

import com.cedarsoftware.util.io.JsonReader;
import com.cedarsoftware.util.io.JsonWriter;

import java.io.IOException;

/**
 * Created by mark on 7/22/14.
 */
public class FileSystemMessage {
    private FileSystemMessageType type;
    private String filename;
    private String toAddress;
    private String fromAddress;
    private String content;
    private Integer version;
    private String nonce;
    
    public String getToAddress() {
        return this.toAddress;
    }

    public String getFromAddress(){
        return this.fromAddress;
    }
    public String getContent() { return this.content; }
    public int getVersion() { return version; }
    public String getFilename() { return this.filename; }
    public FileSystemMessageType getMessageType() { return this.type; }

    public void setFromAddress(String address){
        this.fromAddress = address;
    }
    
    

    public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public void setContent(String content) {
		this.content = content;
	}

	private FileSystemMessage(String filename, FileSystemMessageType type, String toAddress, String fromAddress, String content, Integer version, String nonce) {
        this.filename = filename;
        this.type = type;
        this.toAddress = toAddress;
        this.fromAddress = fromAddress;
        this.content = content;
        this.version = version;
        this.nonce = nonce;
    }
    
    private FileSystemMessage(String filename, FileSystemMessageType type, String toAddress, String fromAddress, String content, Integer version) {
        this.filename = filename;
        this.type = type;
        this.toAddress = toAddress;
        this.fromAddress = fromAddress;
        this.content = content;
        this.version = version;
    }

    public String toJSON() throws IOException {
        return JsonWriter.objectToJson(this);
    }

    public static FileSystemMessage fromJSON(String json) throws IOException {
        return (FileSystemMessage) JsonReader.jsonToJava(json);
    }

    public static FileSystemMessage CreateFileContent(String filename, String toAddress, String fromAddress, String content, int version){
        return new FileSystemMessage(filename, FileSystemMessageType.FILE_CONTENT, toAddress, fromAddress, content, version);
    }

    public static FileSystemMessage CreateRequestLock(String filename, String toAddress, String fromAddress){
        return new FileSystemMessage(filename, FileSystemMessageType.REQUEST_LOCK, toAddress, fromAddress, null, null);
    }

    public static FileSystemMessage CreateAssignVote(String filename, String toAddress, String fromAddress, int version){
        return new FileSystemMessage(filename, FileSystemMessageType.ASSIGN_VOTE, toAddress, fromAddress, null, version);
    }

    public static FileSystemMessage CreateAbort(String filename, String toAddress, String fromAddress){
        return new FileSystemMessage(filename, FileSystemMessageType.ABORT, toAddress, fromAddress, null, null);
    }

    public static FileSystemMessage CreateRetrieveFile(String filename, String toAddress, String fromAddress, int version){
        return new FileSystemMessage(filename, FileSystemMessageType.RETRIEVE_FILE, toAddress, fromAddress, null, version);
    }

    public static FileSystemMessage CreateReleaseLockAck(String filename, String toAddress, String fromAddress, String nonce){
        return new FileSystemMessage(filename, FileSystemMessageType.RELEASE_LOCK_ACK, toAddress, fromAddress, null, null, nonce);
    }

    public static FileSystemMessage CreateSetFile(String filename, String toAddress, String fromAddress, String content, int version){
        return new FileSystemMessage(filename, FileSystemMessageType.SET_FILE, toAddress, fromAddress, content, version);
    }

    public static FileSystemMessage CreateReleaseLock(String filename, String toAddress, String fromAddress, String nonce){
        return new FileSystemMessage(filename, FileSystemMessageType.RELEASE_LOCK, toAddress, fromAddress, null, null, nonce);
    }

     /* TODO: Complete arguments and implementations for the Create{MessageType} methods below.
        They should only include the required properties for that message type.
     */

    public static FileSystemMessage CreateSetFileAck(String filename, String toAddress, String fromAddress, int version){
        return new FileSystemMessage(filename, FileSystemMessageType.SET_FILE_ACK, toAddress, fromAddress, null, version);
    }


}

