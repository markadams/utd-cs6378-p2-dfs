package edu.utdallas.cs6378.dfs.filesystem;

import edu.utdallas.cs6378.dfs.channels.Channel;
import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;
import edu.utdallas.cs6378.dfs.logging.TxLog;
import edu.utdallas.cs6378.dfs.logging.TxLogFile;
import edu.utdallas.cs6378.dfs.logging.TxLogNull;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessageNonce;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessageType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * Created by mark on 7/22/14.
 */
public class FileSystemImpl extends FileSystem implements Runnable {
    public static final int TIMEOUT = 1;
    private static Logger logger = LogManager.getLogger("FS");
    private static Logger replyLogger = LogManager.getLogger("FSR");
    private boolean isClosed = false;
    private Thread incomingThread;
    private LinkedBlockingQueue<FileSystemMessage> replyMessages;
    private String localAddress;
    private String releaseNonce;
    private TxLog transactionLog;


    public FileSystemImpl(int myVotes, String localAddress, Channel channel, List<Peer> peers, int readQuorumSize, int writeQuorumSize, boolean enableTxLog) throws IOException {
        super(myVotes, channel, peers, readQuorumSize, writeQuorumSize);
        this.localAddress = localAddress;
        this.replyMessages = new LinkedBlockingQueue<>();

        this.incomingThread = new Thread(this);
        this.incomingThread.start();

        setReleaseNonce(FileSystemMessageNonce.getNonce());

        if (enableTxLog) {
            transactionLog = new TxLogFile(localAddress + ".log");
        } else {
            transactionLog = new TxLogNull();
        }

        logger.info(String.format("Initializing FS on %s", localAddress));
    }

    /**
     * Gets the local file list (for testing)
     *
     * @return The local file list
     */
    public Map<String, LocalFile> getLocalFiles() {
        return this.files;
    }

    /**
     * Sets the local file list to use the specified file list (for testing)
     *
     * @param files
     */
    public void setLocalFiles(Map<String, LocalFile> files) {
        this.files = new ConcurrentHashMap<>(files);
    }

    @Override
    public FileHandle openFile(String filename, FileMode mode) throws LockAcquisitionException {
        logger.info(String.format("[%s] - Requesting lock", filename));

        // If the file exists, grab the info about our local copy; otherwise create a new local file instance
        LocalFile file = this.files.containsKey(filename) ? this.files.get(filename) : new LocalFile(filename);
        FileHandle handle = new FileHandleImpl(this, mode, filename);

        if (file.isLocked()) {
            // If the file is already locked, throw an exception.
            throw new LockAcquisitionException();
        }

        // Lock the file locally
        file.lock(this.localAddress);

        // Give our votes to ourselves
        int votes = this.localVotes;

        try {
            clearReplyMessages();

            for (Peer p : this.peers) {
                FileSystemMessage msg = FileSystemMessage.CreateRequestLock(filename, p.getAddress(), this.localAddress);
                channel.send(msg);
            }

            int quorumVotes = mode == FileMode.READ ? this.readQuorumSize : this.writeQuorumSize;

            while (votes < quorumVotes) {
                FileSystemMessage msg = this.replyMessages.poll(TIMEOUT, TimeUnit.SECONDS);

                if (msg == null) {
                    throw new LockAcquisitionException("Timed out while waiting for lock confirmation");
                }

                if (msg.getMessageType() != FileSystemMessageType.ASSIGN_VOTE) {
                    continue;
                }

                Peer p = findPeers(msg.getFromAddress());

                // For each ASSIGN_VOTE message that we receive, add the votes to our total
                votes += p.getVotes();

                // If the version on the peer is newer than ours, set max version to their version
                if (msg.getVersion() > file.getMaxVersion()) {
                    file.setMaxVersion(msg.getVersion());
                }

            }

        } catch (LockAcquisitionException ex) {
            try {
                abort(handle);
            } catch (IOException ioex) {
            }
            throw ex;
        } catch (Exception ex) {
            try {
                abort(handle);
            } catch (IOException ioex) {
            }
            throw new LockAcquisitionException("A lock could not be acquired because an exception was thrown.", ex);
        }

        logger.info(String.format("[%s] - Lock acquired", filename));

        // Store the file in the HashMap just in case it is a new file
        this.files.put(filename, file);

        // Return the file handle
        return new FileHandleImpl(this, mode, filename);
    }

    @Override
    public String readFile(FileHandle handle) throws LockAcquisitionException, ReadFailedException, InvalidModeException {
        if (handle.getMode() != FileMode.READ) {
            throw new InvalidModeException();
        }

        logger.info(String.format("[%s] - Requesting read", handle.getFileName()));
        LocalFile file = files.get(handle.getFileName());

        // Optimization: If we already have the max version, return our local content without querying peers
        if (handle.getVersion() == file.getMaxVersion()) {
            logger.info(String.format("[%s] - Read satisfied locally", handle.getFileName()));
            return file.getContent();
        }

        String content = null;

        try {
            clearReplyMessages();

            // Send a RETRIEVE_FILE message to each peer
            for (Peer p : peers) {
                FileSystemMessage msg = FileSystemMessage.CreateRetrieveFile(handle.getFileName(), p.getAddress(), localAddress, file.getMaxVersion());
                channel.send(msg);
            }

            while (content == null) {
                // For each message received back
                FileSystemMessage msg = this.replyMessages.poll(TIMEOUT, TimeUnit.SECONDS);

                // If the peer has the max version, return its content
                if (msg.getVersion() == file.getMaxVersion()) {
                    content = msg.getContent();
                }
            }
        } catch (Exception e) {
            throw new ReadFailedException("The attempt to read the file failed. See the exception for details.", e);
        }

        file.setVersion(file.getMaxVersion());
        file.setContent(content);
        transactionLog.write(handle.getFileName(), FileMode.READ, handle.getVersion(), file.getContent());

        logger.info(String.format("[%s] - Read completed", handle.getFileName()));
        return content;
    }

    @Override
    public void writeFile(FileHandle handle, String content) throws InvalidModeException, WriteFailedException {
        if (handle.getMode() != FileMode.WRITE) {
            throw new InvalidModeException();
        }

        logger.info(String.format("[%s] - Requesting write", handle.getFileName()));
        LocalFile file = files.get(handle.getFileName());
        int votes = this.localVotes;

        // Update our local copy of the file
        int newVersion = file.getMaxVersion() + 1;
        file.setPendingContent(content, newVersion);

        try {
            clearReplyMessages();

            // Send write requests to everyone
            for (Peer p : peers) {
                channel.send(FileSystemMessage.CreateSetFile(
                        handle.getFileName(), p.getAddress(),
                        localAddress, file.getPendingContent(), newVersion));
            }

            int quorumVotes = this.writeQuorumSize;

            while (votes < quorumVotes) {
                // Until we have a write quorum amount of votes...
                FileSystemMessage msg = this.replyMessages.poll(TIMEOUT, TimeUnit.SECONDS);

                if (msg == null) {
                    // If we've timed out, throw an exception
                    throw new WriteFailedException("The write attempt timed out without receiving enough responses");
                }

                if (msg.getMessageType() != FileSystemMessageType.SET_FILE_ACK) {
                    // We don't care about other messages here so just ignore them
                    continue;
                }

                Peer p = findPeers(msg.getFromAddress());

                // For each ASSIGN_VOTE message that we receive, add the votes to our total
                votes += p.getVotes();
            }

        } catch (WriteFailedException e) {
            try {
                abort(handle);
            } catch (IOException ex) {
            }
            ;
            throw e;
        } catch (Exception e) {
            try {
                abort(handle);
            } catch (IOException ex) {
            }
            ;
            throw new WriteFailedException("Failed to write to this file.", e);
        }

        logger.info(String.format("[%s] - Write completed", handle.getFileName()));
    }

    @Override
    public void closeFile(FileHandle handle) throws LockAcquisitionException {

        try {
            LocalFile file = files.get(handle.getFileName());

            if (!file.isLocked()) {
                throw new LockAcquisitionException();
            }

            logger.info(String.format("[%s] - Requesting release lock", handle.getFileName()));
            sendReleaseLock(handle);

            int releaseAckVotes = localVotes;
            int quorumVotes = handle.getMode() == FileMode.WRITE ? this.writeQuorumSize : this.readQuorumSize;

            while (releaseAckVotes < quorumVotes) {
                FileSystemMessage msg = this.replyMessages.poll(TIMEOUT, TimeUnit.SECONDS);

                if (msg == null) {
                    //reset the releaseAcks
                    releaseAckVotes = localVotes;
                    // If we've timed out, resend the release lock message.
                    setReleaseNonce(FileSystemMessageNonce.getNonce());
                    sendReleaseLock(handle);

                    continue;
                }
                // Check the nonce that is sent from the message.
                if (msg.getMessageType() == FileSystemMessageType.RELEASE_LOCK_ACK
                        && releaseNonce.equals(msg.getNonce())) {

                    releaseAckVotes += findPeers(msg.getFromAddress()).getVotes();
                }

            }

            if (file.getPendingContent() != null){
                file.commit();
                transactionLog.write(handle.getFileName(), FileMode.WRITE, handle.getVersion(), file.getContent());
            }

            file.release();

        } catch (LockAcquisitionException ex) {
            try {
                abort(handle);
            } catch (IOException ioex) {
            }
            throw ex;
        } catch (Exception ex) {
            try {
                abort(handle);
            } catch (IOException ioex) {
            }
            ex.printStackTrace();
            throw new LockAcquisitionException("A lock could not be acquired because an exception was thrown.", ex);
        }

        logger.info(String.format("[%s] - Release lock completed", handle.getFileName()));
    }

    @Override
    public void abort(FileHandle handle) throws IOException {
        logger.info(String.format("[%s] - Aborting and releasing lock", handle.getFileName()));

        LocalFile file = this.files.get(handle.getFileName());

        if (file == null) {
            return;
        }

        file.rollback();
        file.release();

        for (Peer p : this.peers) {
            FileSystemMessage msg = FileSystemMessage.CreateAbort(handle.getFileName(), p.getAddress(), this.localAddress);
            channel.send(msg);
        }

    }

    private void sendReleaseLock(FileHandle handle) throws IOException {
        clearReplyMessages();
        for (Peer p : this.peers) {
            FileSystemMessage msg = FileSystemMessage.CreateReleaseLock(handle.getFileName(),
                    p.getAddress(), this.localAddress, releaseNonce);
            channel.send(msg);
        }
    }


    public String getReleaseNonce() {
        return releaseNonce;
    }

    public void setReleaseNonce(String releaseNonce) {
        this.releaseNonce = releaseNonce;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    @Override
    public void close() throws IOException {
        if (this.incomingThread != null) {
            this.incomingThread.interrupt();
        }

        this.channel.close();
        this.transactionLog.close();

        isClosed = true;
    }

    @Override
    public int getVersion(FileHandle handle) {
        LocalFile file = this.files.get(handle.getFileName());

        return file.getVersion();
    }

    @Override
    public boolean isClosed() {
        return isClosed;
    }

    /**
     * Return the number of reply messages waiting
     *
     * @return
     */
    public int getReplyMessageCount() {
        return replyMessages.size();
    }

    private Peer findPeers(String address) {
        for (Peer p : peers) {
            if (address.equals(p.getAddress())) {
                return p;
            }
        }

        return null;
    }

    private void clearReplyMessages() {
        this.replyMessages.clear();
    }

    @Override
    public void run() {
        Logger logger = LogManager.getLogger("FS-In");


        while (true) {
            try {
                // This thread handles incoming messages off the channel
                FileSystemMessage msg = this.channel.recv();

                if (msg == null) {
                    // If we return without a message, it means the channel closed.
                    return;
                }

                switch (msg.getMessageType()) {
                    // Reply messages... blocking methods are waiting on these so we put them into the replyMessages queue
                    case ASSIGN_VOTE:
                    case FILE_CONTENT:
                    case SET_FILE_ACK:
                    case RELEASE_LOCK_ACK:
                        replyMessages.add(msg);
                        break;
                    // Non-reply messages... we need to handle them and reply on this thread
                    case REQUEST_LOCK:
                        processIncomingLockRequest(msg);
                        break;
                    case RETRIEVE_FILE:
                        processIncomingRetrieveFile(msg);
                        break;
                    case SET_FILE:
                        processIncomingSetFile(msg);
                        break;
                    case RELEASE_LOCK:
                        processIncomingReleaseLock(msg);
                        break;
                    case ABORT:
                        processIncomingAbort(msg);
                        break;
                }

            } catch (IOException e) {
                if (!e.getMessage().equals("Channel has already been closed and no more messages remain to be received.")) {
                    e.printStackTrace();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void processIncomingLockRequest(FileSystemMessage msg) throws IOException {
        LocalFile file = files.get(msg.getFilename());

        if (file == null) {
            // If the file doesn't exist yet, create a local reference for it
            file = new LocalFile(msg.getFilename());
            files.put(msg.getFilename(), file);
        }

        // If the file is already locked by someone else, don't reply
        if (file.isLocked()) {
            return;
        }

        try {
            // Lock the file
            file.lock(msg.getFromAddress());
        } catch (LockAcquisitionException e) {
            e.printStackTrace();
            return;
        }

        // Create an ASSIGN_VOTE message and reply to the sender
        replyLogger.info(String.format("[%s] - Processed lock request from %s", msg.getFilename(), msg.getFromAddress()));
        FileSystemMessage reply = FileSystemMessage.CreateAssignVote(msg.getFilename(), msg.getFromAddress(), getLocalAddress(), file.getVersion());
        channel.send(reply);
    }

    private void processIncomingRetrieveFile(FileSystemMessage msg) throws IOException {
        LocalFile file = files.get(msg.getFilename());

        // If the file is not locked or is not locked by the requester, ignore the message
        if (file == null || !file.isLocked() || !file.getLockSource().equals(msg.getFromAddress())) {
            return;
        }

        if (file.getVersion() != msg.getVersion()) {
            // If we don't have the version specified in the message, ignore the msg
            return;
        }

        // Send the file Content back to the requester
        replyLogger.info(String.format("[%s] - Processed read request from %s.", msg.getFilename(), msg.getFromAddress()));

        channel.send(
                FileSystemMessage.CreateFileContent(msg.getFilename(),
                        msg.getFromAddress(), msg.getToAddress(), file.getContent(), file.getVersion()));
    }

    private void processIncomingSetFile(FileSystemMessage msg) throws IOException {
        LocalFile file = files.get(msg.getFilename());

        // If the file is not locked or is not locked by the requester, ignore the message
        if (file == null || !file.isLocked() || !file.getLockSource().equals(msg.getFromAddress())) {
            return;
        }

        file.setPendingContent(msg.getContent(), msg.getVersion());

        replyLogger.info(String.format("[%s] - Processed write request from %s", msg.getFilename(), msg.getFromAddress()));

        FileSystemMessage reply = FileSystemMessage.CreateSetFileAck(msg.getFilename(), msg.getFromAddress(), localAddress, msg.getVersion());
        channel.send(reply);
    }

    private void processIncomingReleaseLock(FileSystemMessage msg) throws IOException {
        LocalFile file = files.get(msg.getFilename());

        // If the file is already locked by someone else, don't reply.\
        if (file != null && file.isLocked() && !file.getLockSource().equals(msg.getFromAddress())) {
            return;
        }

        if (file != null) {
            // If we have a local copy of the file, rollback and release where appropriate
            if (file.getPendingContent() != null) // In WRITE mode
            {
                // Do commit on the file content
                file.commit();
                transactionLog.write(msg.getFilename(), FileMode.WRITE, file.getVersion(), file.getContent());
            }

            if (file.isLocked()) {
                file.release();
            }
        }

        channel.send(FileSystemMessage.CreateReleaseLockAck(msg.getFilename(),
                msg.getFromAddress(), msg.getToAddress(), msg.getNonce()));

        replyLogger.info(String.format("[%s] - Processed release lock request from %s.", msg.getFilename(), msg.getFromAddress()));
    }

    private void processIncomingAbort(FileSystemMessage msg) throws FileNotFoundException {
        LocalFile file = files.get(msg.getFilename());

        // No need to do anything if we don't even have a local copy of the file
        if (file == null) {
            return;
        }

        // If the file is already locked by someone else, don't reply.\
        if (file != null && file.isLocked() && !file.getLockSource().equals(msg.getFromAddress())) {
            return;
        }

        if (file.getPendingContent() != null) // In write mode
        {
            // Do rollback on the file content
            file.rollback();
        }

        if (file.isLocked()) {
            file.release();
        }

        replyLogger.info(String.format("[%s] - Processed abort request from %s.", msg.getFilename(), msg.getFromAddress()));
    }

}
