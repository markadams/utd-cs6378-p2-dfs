package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.filesystem.*;
import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;
import edu.utdallas.cs6378.dfs.channels.LocalChannel;

import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 * Created by mark on 7/22/14.
 */
public class FileHandleTestCase {

    @Test(expected=InvalidModeException.class)
    public void testFileHandleShouldThrowExceptionIfReadInWriteMode() throws InvalidModeException, IOException, LockAcquisitionException, ReadFailedException {
        FileSystem sys = FileSystem.join(1, "localhost:5454", new LocalChannel(), new ArrayList<Peer>(), 10, 10);
        FileHandle handle = new FileHandleImpl(sys, FileMode.WRITE, "test.dat");
        String content = handle.read();
    }

    @Test(expected=InvalidModeException.class)
    public void testFileHandleShouldThrowExceptionIfWriteInReadMode() throws InvalidModeException, IOException, WriteFailedException {
        FileSystem sys = FileSystem.join(1, "localhost:5454", new LocalChannel(), new ArrayList<Peer>(), 10, 10);
        FileHandle handle = new FileHandleImpl(sys, FileMode.READ, "test.dat");
        handle.write("TEST");
    }

}
