package edu.utdallas.cs6378.dfs.channels;

import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;

import java.io.IOException;
import java.util.Collection;

/**
 * Created by mark on 6/22/14.
 */
public abstract class Channel implements Runnable{
    /**
     * Sends the specified message to its recipient
     * @param msg The message to send.
     * @throws IOException
     */
    public abstract void send(FileSystemMessage msg) throws IOException;

    /**
     * Blocks until it receives an incoming message from the channel
     * @return a FileSystemMessage from the channel
     * @throws IOException
     * @throws InterruptedException
     */
    public abstract FileSystemMessage recv() throws IOException, InterruptedException;

    /**
     * Closes the channel and underlying sockets
     * @throws IOException
     */
    public abstract void close() throws IOException;

    /**
     * Returns whether or not the channel is open
     * @return Whether or not the channel is open
     */
    public abstract boolean isOpen();

    /**
     * Sends a collection of FileSystemMessage messages to their recipients
     * @param messages
     * @throws IOException
     */
    public void send(Collection<FileSystemMessage> messages) throws IOException{
        for (FileSystemMessage msg : messages){
            this.send(msg);
        }
    }

}
