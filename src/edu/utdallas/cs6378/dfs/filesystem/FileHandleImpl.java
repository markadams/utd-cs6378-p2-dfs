package edu.utdallas.cs6378.dfs.filesystem;

import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;

import java.io.IOException;

/**
 * Created by mark on 7/22/14.
 */
public class FileHandleImpl implements FileHandle {
    private String filename;
    private FileMode mode;
    private FileSystem fs;
    private boolean closed = false;

    public FileHandleImpl(FileSystem fs, FileMode mode, String filename){
        this.fs = fs;
        this.mode = mode;
        this.filename = filename;
    }

    @Override
    public String read() throws InvalidModeException, IOException, LockAcquisitionException, ReadFailedException {
       if (closed){
           throw new IOException("The file has already been closed.");
       }

       if (mode != FileMode.READ){
           throw new InvalidModeException();
       }

       return fs.readFile(this);
    }

    @Override
    public void write(String content) throws InvalidModeException, IOException, WriteFailedException {
        if (closed){
            throw new IOException("The file has already been closed.");
        }

        if (mode != FileMode.WRITE){
            throw new InvalidModeException();
        }

        fs.writeFile(this, content);
    }

    @Override
    public void close() throws IOException, LockAcquisitionException {
        if (closed){
            throw new IOException("The file has already been closed.");
        }

	    fs.closeFile(this);
		closed = true;
    }

    @Override
    public int getVersion() {
        return fs.getVersion(this);
    }

    @Override
    public FileMode getMode() {
        return mode;
    }

    @Override
    public String getFileName() {
        return filename;
    }

	public String getFilename() {
		return filename;
	}

	public boolean isClosed() {
		return closed;
	}
    
    
}
