package edu.utdallas.cs6378.dfs;

import edu.utdallas.cs6378.dfs.channels.TcpNetworkChannel;
import edu.utdallas.cs6378.dfs.config.ConfigFile;
import edu.utdallas.cs6378.dfs.config.ConfigFileLine;
import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;
import edu.utdallas.cs6378.dfs.filesystem.FileHandle;

import edu.utdallas.cs6378.dfs.filesystem.FileSystem;
import edu.utdallas.cs6378.dfs.filesystem.Peer;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import static edu.utdallas.cs6378.dfs.filesystem.FileMode.*;

public class Main {
    private static Logger logger;

    public static CommandLine ParseCommandLineArguments(String[] args){
        Options opts = new Options();
        Option opt = null;

        opt = OptionBuilder.withLongOpt("config-file")
                .hasArg(true)
                .isRequired(true)
                .withDescription("The config file name")
                .create();

        opts.addOption(opt);

        CommandLineParser parser = new BasicParser();

        try {
            return parser.parse(opts, args);
        } catch (MissingOptionException ex) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("cs6378-p2-dfs", opts, true);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        System.exit(1);
        return null;
    }

    public static void processOperations(FileSystem fs, Iterable<ConfigFileLine> operations) throws InterruptedException {
        for (ConfigFileLine op : operations) {
            int attempt = 0;

            logger.trace(op.toString());
            boolean success = false;

            while (!success) {
                if (attempt != 0){
                    // Exponential backoff
                    int wait = (int)Math.floor(Math.pow(2, attempt)) * 100;
                    logger.warn(String.format("Retrying operation (attempt #%d) in %d ms", attempt + 1, wait));
                    Thread.sleep(wait);
                }

                FileHandle handle = null;

                try {
                    handle = fs.openFile(op.getFilename(), op.getOperation());

                    switch (op.getOperation()) {
                        case READ:
                            String content = handle.read();
                            break;
                        case WRITE:
                            handle.write(op.getContent());
                            break;
                    }

                    handle.close();
                    success = true;
                } catch (LockAcquisitionException ex) {
                    logger.warn(ex.getMessage());
                } catch (ReadFailedException | WriteFailedException ex) {
                    logger.error(ex.getMessage());
                } catch (Exception ex) {
                    logger.error("An unexpected exception occurred.", ex);
                }

                if (success){
                    logger.info(String.format("[%s] - %s operation completed. V: %d", op.getFilename(), op.getOperation(), handle.getVersion()));
                }

                attempt = attempt + 1;
            }

            // This was added to make it a little easier for messages to overlap
            Thread.sleep(200);
        }
    }

    public static void pressEnterTo(String verb){
        System.out.println("Press 'Enter' to " + verb + "...");
        Scanner in = new Scanner(System.in);
        in.nextLine();
    }

    public static void main(String[] args) throws Exception {
        // This will eventually parse command line arguments once we have them
        CommandLine arguments = ParseCommandLineArguments(args);
        ConfigFile config = new ConfigFile(arguments.getOptionValue("config-file"));

        System.setProperty("log_filename", config.getAddress() + ".out.log");
        logger = LogManager.getLogger("App");

        // Initialize file system
        FileSystem fs = FileSystem.join(config.getVotes(), config.getAddress(), config.getPort(), config.getPeers(), config.getR(), config.getW(), true);

        processOperations(fs, config);

        pressEnterTo("quit");
        fs.close();
        System.exit(0);
    }
}
