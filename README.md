# CS 6378 - Advanced Operating Systems - Project 2
## Fault Tolerant Distributed File System using Static Voting
### University of Texas at Dallas

Mark Adams <mark@markadams.me, mla104020@utdallas.edu>  
Tejashree Mohile <tejumohile@gmail.com, txm122130@utdallas.edu>  

### Instructions
To compile: `ant compile jar`  
To run unit tests: `ant test`  

To run the app:   
`java -jar jar/cs6378-p2-dfs.jar --config-file data/set0-localhost:5454`  
`java -jar jar/cs6378-p2-dfs.jar --config-file data/set0-localhost:5455`   
`java -jar jar/cs6378-p2-dfs.jar --config-file data/set0-localhost:5456`   

### Related documents:
* [Testing Process](src/master/TESTING.md)
* [Project Requirements](src/master/project-2-desc.pdf)
* [Static Voting Protocol](src/master/static-voting.pdf)