package edu.utdallas.cs6378.dfs.filesystem;

import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;


/**
 * Represents a local copy of a file on this node
 */
public class LocalFile {
    // The filename of the file
    private String filename;

    // The current file version
    private int version;

    // The current file content
    private String content;

    // Pending (uncommitted) content
    private String pendingContent;
    private Integer pendingVersion;

    // Whether or not the file has been locked
    private boolean locked;

    // The address of the node that has the file locked
    private String lockSource;

    // If we have the file locked, the maximum version of the file that exists in the FS
    private Integer maxVersion;

    public LocalFile(String filename){
        this.filename = filename;
        this.version = 0;
        this.maxVersion = 0;
    }

    public synchronized int getVersion(){
        return version;
    }

    public synchronized void setMaxVersion(int value) {
        maxVersion = value;
    };

    public synchronized int getMaxVersion(){
        return maxVersion;
    }

    public synchronized void setVersion(int version){
        if (version <= this.version){
            throw new IllegalArgumentException("Version must be greater than the current version");
        }

        if (version < this.maxVersion){
            throw new IllegalArgumentException("Version must be greater than the maximum version seen so far.");
        }

        this.version = version;

        if (version > this.maxVersion){
            setMaxVersion(version);
        }
    }

    public synchronized String getContent(){
        return content;
    }
    
    public synchronized void setContent(String content) {
		this.content = content;
	}

	public synchronized String getPendingContent(){
        return pendingContent;
    }

    public synchronized int getPendingVersion() { return pendingVersion; }

    public synchronized boolean isLocked(){
        return locked;
    }

    public synchronized String getLockSource(){
        return lockSource;
    }

    public synchronized void release(){
        locked = false;
        lockSource = null;
    }

    public synchronized void setPendingContent(String newContent, int version){
        this.pendingContent = newContent;
        this.pendingVersion = version;
    }

    public synchronized  void commit(){
        if (this.pendingContent != null){
            setVersion(this.pendingVersion);
            this.content = this.pendingContent;
            this.pendingContent = null;
            this.pendingVersion = null;
        }
    }

    public synchronized  void rollback(){
        this.pendingContent = null;
        this.pendingVersion = null;
    }

    public synchronized void lock(String lockSource) throws LockAcquisitionException {
        if (isLocked()){
            throw new LockAcquisitionException();
        }

        this.locked = true;
        this.lockSource = lockSource;
    }



}
