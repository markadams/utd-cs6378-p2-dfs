package edu.utdallas.cs6378.dfs.errors;

public class WriteFailedException extends Exception {

	public WriteFailedException(String message){
        super(message);
    }
    public WriteFailedException(String message, Throwable cause){
        super(message, cause);
    }
}
