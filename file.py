class File:

    def __init__(self, filename):
        self.__filename = filename;
        self.__versions = {};
        self.__quorumVotes = {};

    def addVersions(self, versionNumber, hash):
        self.__versions[versionNumber] = hash;

    def versionExists(self, versionNumber):
        if versionNumber in self.__versions:
            return self.__versions[versionNumber];
	return False;

    def addQuromVote(self, versionNumber, vote):
        if versionNumber in self.__quorumVotes:
            self.__quorumVotes[versionNumber] = self.__quorumVotes[versionNumber] + vote;
        else:
            self.__quorumVotes[versionNumber] = vote;

    ''' For all the versions of a file, must have valid quorum. '''
    def isValidQuorum(self, w):
        for version in self.__quorumVotes:
            '''print self.__filename + " " + version + " " + str(self.__quorumVotes[version]);'''
            if int(version) != 0 and w > self.__quorumVotes[version]:
                print 'Filename: {0}, Version: {1}, Total Votes: {2}'.format(self.__filename, version,self.__quorumVotes[version]);
                return False;
        return True;
