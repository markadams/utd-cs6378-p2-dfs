# Testing Process

## Unit Testing

As we developed our application, we created a number of unit tests to verify that key parts of our protocol behaved properly. This enabled us to refactor and improve our program was ensuring that the protocols core functionality was not impacted by any changes that we made.

To run unit tests:
`ant test`

## Result Verification
Each process will output a transaction log file in the format of <host>:<port>.log. This file will record each time that the local state of a process was updated either through performing a write operation or through receiving a new value during a read.

Our result verification script checks the entries from the transaction logs for the following:

* Each filename and version pair should across all nodes should have the same
  content hash on all nodes where the version is found. 

* Each version should be found on at least enough nodes to satisfy W votes.

* On the same node, a file with version v+1 should always come after a file
  with version v of the same filename.

To run result verification tests:
`verify_results.py <tx-log-1> <tx-log-2> ... <cfg-file>`
