package edu.utdallas.cs6378.dfs.filesystem;

import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;

import java.io.IOException;

/**
 * Represents a handle for a file in the DFS
 */
public interface FileHandle {
    /**
     * Reads the most recent file content from the DFS
     * @return The most recent version of the file
     * @throws InvalidModeException
     */
    public String read() throws InvalidModeException, IOException, LockAcquisitionException, ReadFailedException;

    /**
     * Writes a new version of the file content to the DFS
     * @param content The file content to write
     * @throws InvalidModeException
     * @throws WriteFailedException 
     */
    public void write(String content) throws InvalidModeException, IOException, WriteFailedException;

    /**
     * Closes a file handle and releases the locks
     * @throws LockAcquisitionException 
     */
    public void close() throws IOException, LockAcquisitionException;

    /**
     * Returns the most recent version number of the file
     * @return The most recent version number
     */
    public int getVersion();

    /**
     * The mode of the file handle
     * @return The read/write mode of the file handle
     */
    public FileMode getMode();

    /**
     * Returns the name of the file
     * @return The filename
     */
    public String getFileName();
}
