package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.filesystem.FileMode;
import edu.utdallas.cs6378.dfs.logging.TxLogFile;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;

/**
 * Created by mark on 8/3/14.
 */
public class TxLogTestCase {
    private TxLogFile log;
    private ByteArrayOutputStream stream;

    @Before
    public void setup() throws NoSuchAlgorithmException {
       stream = new ByteArrayOutputStream();
       log = new TxLogFile(stream);
    }

    @Test
    public void testHash() throws IOException, NoSuchAlgorithmException {
        String content = "Hello World!";
        String expected = "ed076287532e86365e841e92bfc50d8c";

        log.write("testfile.txt", FileMode.READ, 10, content);
        log.close();

        String[] result = stream.toString().trim().split("\t");

        assertEquals(expected, result[2]);



    }
}
