package edu.utdallas.cs6378.dfs.logging;

import edu.utdallas.cs6378.dfs.filesystem.FileMode;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;

/**
 * Created by mark on 8/3/14.
 */
public class TxLogFile implements TxLog {
    protected BufferedWriter writer;

    public TxLogFile(String logFilename) throws IOException {
        writer = new BufferedWriter(new FileWriter(logFilename));
    }

    public TxLogFile(OutputStream str) {
        writer = new BufferedWriter(new OutputStreamWriter(str));
    }

    public synchronized void write(String filename, FileMode op, int version, String content) {
        if (content == null){
            content = "";
        }

        String hash = DigestUtils.md5Hex(content);

        try {
            String operation;
            switch (op){
                case READ:
                    operation = "R";
                    break;
                case WRITE:
                    operation = "W";
                    break;
                default:
                    operation = "?";
            }


            writer.write(String.format("%s\t%d\t%s\t%s\n", filename, version, hash, operation));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() throws IOException {
        writer.close();
    }

}
