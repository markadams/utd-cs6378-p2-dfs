package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.channels.LocalChannel;
import edu.utdallas.cs6378.dfs.channels.TcpNetworkChannel;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.filesystem.*;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessage;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessageNonce;
import edu.utdallas.cs6378.dfs.messages.FileSystemMessageType;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static junit.framework.TestCase.*;

/**
 * Created by mark on 7/22/14.
 */
public class FileSystemTestCase {
    private LocalChannel channel;
    private FileSystemImpl fs;
    private Peer peer1 = new Peer(new InetSocketAddress("127.0.0.2", TcpNetworkChannel.DEFAULT_TCP_PORT), 2);
    private Peer peer2 = new Peer(new InetSocketAddress("127.0.0.3", TcpNetworkChannel.DEFAULT_TCP_PORT), 3);
    private ArrayList<Peer> peers;
    private ConcurrentHashMap<String, LocalFile> files;

    private int READ_QUORUM = 3;
    private int WRITE_QUORUM = 4;
    private int WAIT_TIME = 500;

    @Before
    public void setup() throws IOException {
        channel = new LocalChannel();

        peers = new ArrayList<>();
        peers.add(peer1);
        peers.add(peer2);

        files = new ConcurrentHashMap<String, LocalFile>();
        LocalFile file = new LocalFile("testfile.txt");
        file.setPendingContent("This is a test file!", 10);
        file.commit();
        files.put("testfile.txt", file);

        fs = new FileSystemImpl(1, "127.0.0.1:5454", channel, peers, READ_QUORUM, WRITE_QUORUM, false);
        fs.setLocalFiles(files);

    }

    @Test(expected = IllegalArgumentException.class)
    public void fsShouldEnforceConstraintOnW() throws IOException {
        // W > (V / 2)
        int totalVotes = 1;

        for (Peer p : peers){
            totalVotes += p.getVotes();
        }

        int w = totalVotes / 2;

        fs = new FileSystemImpl(1, "127.0.0.1:5454", channel, peers, READ_QUORUM, w, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fsShouldEnforceRplusWisGreaterThanV() throws IOException {
        // r + w > V
        int totalVotes = 1;

        for (Peer p : peers){
            totalVotes += p.getVotes();
        }

        int read_quorum = totalVotes / 2 - 1;
        int write_quorum = totalVotes / 2 + 1;

        fs = new FileSystemImpl(1, "127.0.0.1:5454", channel, peers, read_quorum, write_quorum, false);
    }

    @Test
    public void fsShouldPlaceReplyMessagesIntoReplyMessageList() throws InterruptedException {

        assertEquals(0, fs.getReplyMessageCount());

        FileSystemMessage incomingMessage = FileSystemMessage.CreateAssignVote("test.txt","testAddress", "testAddress", 1);
        channel.IncomingMessages.put(incomingMessage);

        Thread.sleep(WAIT_TIME);

        assertEquals(1, fs.getReplyMessageCount());
    }

    @Test
    public void fsShouldNotPlaceNonReplyMessagesIntoReplyMessageList() throws InterruptedException {

        assertEquals(0, fs.getReplyMessageCount());

        FileSystemMessage incomingMessage = FileSystemMessage.CreateRequestLock("test.txt", "testAddress", "testAddress");
        channel.IncomingMessages.put(incomingMessage);

        Thread.sleep(WAIT_TIME);

        assertEquals(0, fs.getReplyMessageCount());
    }

    @Test
    public void fsShouldSendLockRequestWhenOpenFile() throws InterruptedException {
        final FileSystem fs = this.fs;


        try{
            fs.openFile("testfile.txt", FileMode.READ);
        }catch (Exception ex){

        }

        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
                if (msg.getMessageType() == FileSystemMessageType.REQUEST_LOCK && msg.getToAddress().equals(p.getAddress().toString())){
                    match = true;
                    break;
                }
            }

            assertTrue(match);
        }
    }

    @Test(expected = LockAcquisitionException.class)
    public void fsThrowExceptionOnOpenFileIfLockNotAcquired() throws InterruptedException, LockAcquisitionException {
        final FileSystem fs = this.fs;

        fs.openFile("testfile.txt", FileMode.READ);
    }

    @Test
    public void fsShouldSendAbortWhenOpenFileTimesOut() throws InterruptedException {
        final FileSystem fs = this.fs;

        // Try opening the file
        try{
            fs.openFile("testfile.txt", FileMode.READ);
        }catch (Exception ex){

        }

        // Verify that a ABORT message was sent to each peer
        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
                if (msg.getMessageType() == FileSystemMessageType.ABORT && msg.getToAddress().equals(p.getAddress().toString())){
                    match = true;
                    break;
                }
            }

            assertTrue(match);
        }
    }
    
    /*
     * This test if randomly one quorum vote is not received for REQUEST_LOCK sent,
     * then ABORT should be sent to all the peers.
     */    
    @Test
    public void fsShouldSendAbortWhenNotEnoughQuorumVotesReceived() throws InterruptedException, IOException {
        String filename = "testfile.txt";
    	LocalFile file = fs.getLocalFiles().get(filename);

        // Try opening the file
        OpenFileTestThread r = new OpenFileTestThread(fs,filename, FileMode.READ);

        Thread t = new Thread(r);
        t.start();

        t.join();

        // Verify that a REQUEST_LOCK message was sent to each peer
        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
            	System.out.println(msg.toJSON().toString());
                if (msg.getMessageType() == FileSystemMessageType.REQUEST_LOCK && msg.getToAddress().equals(p.getAddress().toString())){
                    match = true;
                    channel.OutgoingMessages.remove(msg);
                    break;
                }
            }

            assertTrue(match);
        }

        Thread.sleep(WAIT_TIME);

        Random rand = new Random();
        int randomFailedAssignVote = rand.nextInt(READ_QUORUM);
        for (int i = 0; i < peers.size(); i++) {
        	if(i != randomFailedAssignVote)
            {
        		FileSystemMessage msg = FileSystemMessage.CreateAssignVote(filename, 
        				peers.get(i).getAddress(), fs.getLocalAddress(), file.getVersion());            
        		channel.IncomingMessages.add(msg);
            }
        }

        assertEquals(peers.size(), channel.OutgoingMessages.size());
        
        
    }
    
    @Test
    public void fsShouldSendAssignVoteWhenRequestLockReceivedForUnlockedFile() throws InterruptedException {
        FileSystemMessage msg = FileSystemMessage.CreateRequestLock("testfile.txt", fs.getLocalAddress().toString(), peer1.getAddress().toString());
        channel.IncomingMessages.put(msg);

        Thread.sleep(WAIT_TIME);

        assertEquals(1, channel.OutgoingMessages.size());

        FileSystemMessage replyMsg = channel.OutgoingMessages.take();
        assertEquals(FileSystemMessageType.ASSIGN_VOTE, replyMsg.getMessageType());
        assertEquals(msg.getFilename(), replyMsg.getFilename());
        assertEquals(msg.getFromAddress(), replyMsg.getToAddress());
        assertEquals(msg.getToAddress(), replyMsg.getFromAddress());
    }

    @Test
    public void fsShouldStoreMaxVersionFromAssignVoteReceived() throws LockAcquisitionException, InterruptedException {
        Runnable r = new Runnable(){
            @Override
            public void run() {
                try {
                    fs.openFile("testfile.txt", FileMode.READ);
                } catch (LockAcquisitionException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();

        final int MAX_VERSION = 10;
        FileSystemMessage reply = FileSystemMessage.CreateAssignVote("testfile.txt", fs.getLocalAddress().toString(), peer1.getAddress().toString(), MAX_VERSION);
        channel.IncomingMessages.put(reply);

        t.join();

        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");

        assertEquals(MAX_VERSION, file.getMaxVersion());
    }

    @Test
    public void fsShouldSendRetrieveFileOnRead() throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        final int MAX_VERSION = 12;
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress().toString());

        file.setMaxVersion(MAX_VERSION);
        final FileHandle handle = new FileHandleImpl(fs, FileMode.READ, "testfile.txt");

        Runnable r = new Runnable(){
            @Override
            public void run() {
                try {
                    fs.readFile(handle);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
        t.join();

        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
                if (msg.getMessageType() == FileSystemMessageType.RETRIEVE_FILE
                        && msg.getToAddress().equals(p.getAddress().toString())
                        && msg.getVersion() == MAX_VERSION){
                    match = true;
                    break;
                }
            }

            assertTrue(match);
        }
    }

    @Test
    public void fsShouldNotSendRetrieveFileOnReadIfMaxVersionIsLocal() throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        final int MAX_VERSION = 12;
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress().toString());

        // Set the file's current local version to the max version
        file.setVersion(MAX_VERSION);

        final FileHandle handle = new FileHandleImpl(fs, FileMode.READ, "testfile.txt");

        // Attempt to read the file
        Runnable r = new Runnable(){
            @Override
            public void run() {
                try {
                    fs.readFile(handle);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
        t.join();

        // Verify no messages were sent on the channel
        assertEquals(0, channel.OutgoingMessages.size());
    }

    @Test
    public void fsShouldReplyWithFileContentIfRecvRetrieveFileForLocalVersion() throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked by peer1
        Map<String, LocalFile> localFiles = fs.getLocalFiles();

        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress().toString());

        FileSystemMessage retrieveFileMsg = FileSystemMessage.CreateRetrieveFile("testfile.txt", fs.getLocalAddress().toString(), peer1.getAddress().toString(), file.getVersion());
        channel.IncomingMessages.put(retrieveFileMsg);

        Thread.sleep(WAIT_TIME);

        assertEquals(1, channel.OutgoingMessages.size());
        FileSystemMessage reply = channel.OutgoingMessages.take();

        assertEquals(FileSystemMessageType.FILE_CONTENT, reply.getMessageType());
        assertEquals(fs.getLocalAddress().toString(), reply.getFromAddress());
        assertEquals(peer1.getAddress().toString(), reply.getToAddress());
        assertEquals(file.getVersion(), reply.getVersion());
        assertEquals(file.getContent(), reply.getContent());
    }

    @Test
    public void fsShouldReplyWithReleaseLockAckIfRecvReleaseLockMsgAndFileLocked() throws LockAcquisitionException, InterruptedException {
        Map<String, LocalFile> localFiles = fs.getLocalFiles();

        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress().toString());
        String testNonce = FileSystemMessageNonce.getNonce();
        FileSystemMessage retrieveFileMsg = FileSystemMessage.CreateReleaseLock("testfile.txt", fs.getLocalAddress().toString(), peer1.getAddress().toString(), testNonce);
        channel.IncomingMessages.put(retrieveFileMsg);

        Thread.sleep(WAIT_TIME);
        assertEquals(1, channel.OutgoingMessages.size());
        FileSystemMessage reply = channel.OutgoingMessages.take();

        assertEquals(FileSystemMessageType.RELEASE_LOCK_ACK, reply.getMessageType());
        assertEquals(testNonce, reply.getNonce());
        assertEquals(fs.getLocalAddress().toString(), reply.getFromAddress());
        assertEquals(peer1.getAddress().toString(), reply.getToAddress());

    }

    @Test
    public void fsShouldReplyWithReleaseLockAckIfRecvReleaseLockMsgAndFileNotLocked() throws LockAcquisitionException, InterruptedException {
        Map<String, LocalFile> localFiles = fs.getLocalFiles();

        LocalFile file = localFiles.get("testfile.txt");
        assertEquals(false, file.isLocked());
        String testNonce = FileSystemMessageNonce.getNonce();
        
        FileSystemMessage retrieveFileMsg = FileSystemMessage.CreateReleaseLock("testfile.txt", fs.getLocalAddress(), peer1.getAddress(),testNonce);
        channel.IncomingMessages.put(retrieveFileMsg);

        Thread.sleep(WAIT_TIME);
        assertEquals(1, channel.OutgoingMessages.size());
        FileSystemMessage reply = channel.OutgoingMessages.take();

        assertEquals(FileSystemMessageType.RELEASE_LOCK_ACK, reply.getMessageType());
        assertEquals(testNonce, reply.getNonce());
        assertEquals(fs.getLocalAddress().toString(), reply.getFromAddress());
        assertEquals(peer1.getAddress().toString(), reply.getToAddress());

    }

    @Test
    public void fsShouldStorePendingNewVersionWhenReceiveWrite() throws LockAcquisitionException, InterruptedException {
        Map<String, LocalFile> localFiles = fs.getLocalFiles();

        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress());

        FileSystemMessage writeMessage = FileSystemMessage.CreateSetFile("testfile.txt", fs.getLocalAddress(), peer1.getAddress(), "New content", file.getVersion() + 1);
        try
        {
        	//Must get a null pointer exception if there is no value set to pending version.
        	assertNull((Integer)file.getPendingVersion());
        	//If not null then the test fails.
        	assertFalse(true);
        } catch(NullPointerException ex)
        {
        	assertTrue(true);
        }
        
        channel.IncomingMessages.put(writeMessage);

        Thread.sleep(WAIT_TIME);

        assertEquals((Integer)writeMessage.getVersion(), (Integer)file.getPendingVersion());
    }

    @Test
	public void fsShouldSendReleaseLockAckWhenRequestLockReceivedForLockedFile()
			throws InterruptedException {
		String testNonce = FileSystemMessageNonce.getNonce();
		FileSystemMessage msg = FileSystemMessage.CreateReleaseLock(
				"testfile.txt", fs.getLocalAddress().toString(), peer1
						.getAddress().toString(), testNonce);
		channel.IncomingMessages.put(msg);
		Thread.sleep(WAIT_TIME);

		assertEquals(1, channel.OutgoingMessages.size());
		FileSystemMessage replyMsg = channel.OutgoingMessages.take();
		assertEquals(FileSystemMessageType.RELEASE_LOCK_ACK,
				replyMsg.getMessageType());
		assertEquals(msg.getFilename(), replyMsg.getFilename());
		assertEquals(msg.getFromAddress(), replyMsg.getToAddress());
		assertEquals(msg.getToAddress(), replyMsg.getFromAddress());
		assertEquals(testNonce, replyMsg.getNonce());

	}
    
    @Test
	public void fsShouldUnlockLocalFileOnReceivingAbortMessage()
			throws InterruptedException, LockAcquisitionException {
    	Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress());
		FileSystemMessage msg = FileSystemMessage.CreateAbort(
				"testfile.txt", fs.getLocalAddress().toString(), peer1
						.getAddress().toString());
		channel.IncomingMessages.put(msg);
		Thread.sleep(WAIT_TIME);

		assertFalse(file.isLocked());
	}
    
    @Test
	public void fsShouldUnlockLocalFileOnReceivingReleaseLockMessageForReads()
			throws InterruptedException, LockAcquisitionException {
    	LocalFile file = fs.getLocalFiles().get("testfile.txt");
        file.lock(peer1.getAddress());
        
        String testNonce = FileSystemMessageNonce.getNonce();
		FileSystemMessage msg = FileSystemMessage.CreateReleaseLock(
				"testfile.txt", fs.getLocalAddress().toString(), peer1
						.getAddress().toString(), testNonce);
		channel.IncomingMessages.put(msg);
		Thread.sleep(WAIT_TIME);

		assertFalse(file.isLocked());
	}

    
    @Test
    public void fsShouldNotSendSetFileOnRead() 
    		throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress());

        int expectedVersion = file.getVersion() + 1;
        final FileHandle handle = new FileHandleImpl(fs, FileMode.READ, "testfile.txt");
        
        Runnable r = new Runnable(){
        	private String fileContent = null;
        	
            @Override
            public void run() {
                try {
                    fileContent = fs.readFile(handle);
                    assertNotNull(fileContent);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
        t.join();
        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
                if (msg.getMessageType() == FileSystemMessageType.SET_FILE
                        && msg.getToAddress().equals(p.getAddress().toString())
                        && msg.getVersion() == expectedVersion){
                    match = true;
                    break;
                }
            }

            // There should not be any SET_FILE message in the outgoing channels
            assertFalse(match);
        }
    }
    
    @Test
    public void fsShouldSendSetFileOnWrite() 
    		throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress());

        int expectedVersion = file.getVersion() + 1;
        final FileHandle handle = new FileHandleImpl(fs, FileMode.WRITE, "testfile.txt");

        Runnable r = new Runnable(){
            @Override
            public void run() {
                try {
                    fs.writeFile(handle, "This is brand NEW content!");
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(r);
        t.start();
        t.join();

        for (Peer p : peers){
            boolean match = false;

            for (FileSystemMessage msg : channel.OutgoingMessages){
                if (msg.getMessageType() == FileSystemMessageType.SET_FILE
                        && msg.getToAddress().equals(p.getAddress().toString())
                        && msg.getVersion() == expectedVersion){
                    match = true;
                    break;
                }
            }

            assertTrue(match);
        }
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void fsShouldReplyWithSetFileAckOnReceivingSetFile() 
    		throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        LocalFile file = fs.getLocalFiles().get("testfile.txt");
        file.lock(peer1.getAddress().toString());
        int expectedVersion = file.getVersion()+1;
        String newContent = "New File Content is here" ;
        FileSystemMessage msg = FileSystemMessage.CreateSetFile(
				"testfile.txt", fs.getLocalAddress().toString(), peer1
						.getAddress().toString(),newContent , expectedVersion);
		channel.IncomingMessages.put(msg);
        Thread.sleep(WAIT_TIME);
      
        
        assertTrue(file.isLocked());
        assertEquals(1, channel.OutgoingMessages.size());
        FileSystemMessage replyMsg = channel.OutgoingMessages.take();
		assertEquals(FileSystemMessageType.SET_FILE_ACK,
				replyMsg.getMessageType());
		assertEquals(msg.getFilename(), replyMsg.getFilename());
		assertEquals(msg.getFromAddress(), replyMsg.getToAddress());
		assertEquals(msg.getToAddress(), replyMsg.getFromAddress());
		
		//Check what properties the localFile has....
		assertEquals(newContent, file.getPendingContent());
		assertNotSame(newContent, file.getContent());
		assertNotSame(expectedVersion, file.getVersion());
		assertEquals(expectedVersion, file.getPendingVersion());
		assertNotSame(expectedVersion, file.getMaxVersion());
    }
    
    @Test
    public void fsShouldSendFileContentOnReceivingRetrieveFile() throws LockAcquisitionException, InterruptedException {
    	// Set the stage for the test by assuming the file is already locked
        LocalFile file = fs.getLocalFiles().get("testfile.txt");
        file.lock(peer1.getAddress().toString());   	
        
        
        FileSystemMessage msg = FileSystemMessage.CreateRetrieveFile("testfile.txt",
        		        fs.getLocalAddress().toString(), peer1
						.getAddress().toString(), file.getVersion());
		channel.IncomingMessages.put(msg);
		Thread.sleep(WAIT_TIME);

		assertTrue(file.isLocked());
        assertEquals(1, channel.OutgoingMessages.size());
        FileSystemMessage replyMsg = channel.OutgoingMessages.take();
		assertEquals(FileSystemMessageType.FILE_CONTENT,
				replyMsg.getMessageType());
		assertEquals(msg.getFilename(), replyMsg.getFilename());
		assertEquals(msg.getFromAddress(), replyMsg.getToAddress());
		assertEquals(msg.getToAddress(), replyMsg.getFromAddress());
		assertNotNull(replyMsg.getContent());
    }
    
    @Test
    public void fsShouldNotSendFileContentOnReceivingRetrieveFileIfOldVersion() throws LockAcquisitionException, InterruptedException {
    	// Set the stage for the test by assuming the file is already locked
        LocalFile file = fs.getLocalFiles().get("testfile.txt");
        file.lock(fs.getLocalAddress());         
        
        FileSystemMessage msg = FileSystemMessage.CreateRetrieveFile("testfile.txt",
        		        fs.getLocalAddress().toString(), peer1
						.getAddress().toString(), file.getVersion()+10);
		channel.IncomingMessages.put(msg);
		Thread.sleep(WAIT_TIME);

		assertTrue(file.isLocked());
        assertEquals(0, channel.OutgoingMessages.size());       
    }
    
    
    @SuppressWarnings("deprecation")
	@Test
    public void fsShouldNotLockAlreadyLockedFile()  {
		LocalFile file = fs.getLocalFiles().get("testfile.txt");

		try {
			file.lock(fs.getLocalAddress());
			assertTrue(file.isLocked());
			file.lock(peer1.getAddress());
		} catch (LockAcquisitionException ex) {
			assertEquals(file.getLockSource(), fs.getLocalAddress());
		}
    }
    
    
    @Test(timeout=10000)
    public void fsShouldReturnIfQuroumVotesReceived() 
    		throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress());

        final FileHandle handle = new FileHandleImpl(fs, FileMode.WRITE, "testfile.txt");
       
        CloseFileTestThread r = new CloseFileTestThread(handle);

        Thread t = new Thread(r);
        t.start();
        Thread.sleep(WAIT_TIME);
        for (Peer p : peers) {
        	
            FileSystemMessage msg = FileSystemMessage.CreateReleaseLockAck(handle.getFileName(), fs.getLocalAddress(), p.getAddress(), fs.getReleaseNonce());
            channel.IncomingMessages.add(msg);
        }

        t.join();


        assertTrue(r.getResult());
    }

    @Test(timeout=10000)
    public void fsShouldUnlockLocalFileOnCloseIfQuroumVotesReceived() 
    		throws LockAcquisitionException, InterruptedException {
        // Set the stage for the test by assuming the file is already locked
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(fs.getLocalAddress());

        final FileHandle handle = new FileHandleImpl(fs, FileMode.WRITE, "testfile.txt");

        CloseFileTestThread r = new CloseFileTestThread(handle);

        assertTrue(file.isLocked());

        Thread t = new Thread(r);
        t.start();

        FileSystemMessage releaseLock = channel.OutgoingMessages.take();

        for (Peer p : peers) {
            FileSystemMessage msg = FileSystemMessage.CreateReleaseLockAck(handle.getFileName(), fs.getLocalAddress(), p.getAddress(),releaseLock.getNonce());
            channel.IncomingMessages.add(msg);
        }

        t.join();


        assertFalse(file.isLocked());
    }

    @Test
    public void fsCloseShouldCloseChannel() throws IOException {
        assertTrue(channel.isOpen());

        fs.close();

        assertFalse(channel.isOpen());
    }

    @Test
    public void fsReleaseMsgShouldNotReleaseIfSenderDoesNotHoldLock() throws LockAcquisitionException, InterruptedException {
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress());

        FileSystemMessage msg = FileSystemMessage.CreateReleaseLock(
                "testfile.txt", fs.getLocalAddress().toString(), peer2.getAddress(), FileSystemMessageNonce.getNonce());

        channel.IncomingMessages.put(msg);
        Thread.sleep(WAIT_TIME);

        assertTrue(file.isLocked());


    }

    @Test
    public void fsAbortMsgShouldNotAbortIfSenderDoesNotHoldLock() throws LockAcquisitionException, InterruptedException {
        Map<String, LocalFile> localFiles = fs.getLocalFiles();
        LocalFile file = localFiles.get("testfile.txt");
        file.lock(peer1.getAddress());


        FileSystemMessage msg = FileSystemMessage.CreateAbort(
                "testfile.txt", fs.getLocalAddress().toString(), peer2.getAddress());

        channel.IncomingMessages.put(msg);
        Thread.sleep(WAIT_TIME);

        assertTrue(file.isLocked());


    }
}
